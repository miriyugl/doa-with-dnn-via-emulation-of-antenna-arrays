% This file generates the testing data in the  SNR range -16 to 10 dB. This file generates a single
% dateset file for each training SNR for each angle range. Here, we consider  five angle ranges (0-25), (10-35),(20-45),(30-55) and (40-65).
% Testing data generated from this code is used to test the DNN (The low antenna setup data (Y_L_test_ori_R,Y_L_test_ori_C) is the input to the trained DNN).


clear;clc;
rng('shuffle');
tic

 testing_data_size     =    15000; %test data size
 
 
 col_norm=1;
snapshots=150;
 N=10;
 M=16;
 P_L=1;
 P_H=1;
 plotfigure=0
angle_start_loop=0:10:40;
angle_end_loop=10:10:50;

snr_loop=-16:2:10; % testing SNR

for snr_loop_index=1:length(snr_loop)
snr=snr_loop(snr_loop_index) % TESTING SNR


for angle_loop=1:length(angle_start_loop) 
for ant_loop=1:length(N) 

 step=5; % angle step of targets
   
M_H=M(ant_loop); %Tx antennas
N_H=M(ant_loop); %RX antennas
M_L=N(ant_loop); %Tx antennas
N_L=N(ant_loop); %RX antennas
angle_start=angle_start_loop(angle_loop)
angle_end=angle_end_loop(angle_loop)
Target=4; % number of targets
L_Test=snapshots; % number of data points 
trials=testing_data_size/snapshots % no of trialstrials=
%  trials=1

THETA_test=[];
Y_L_test=[];
Y_L_test_norm=[];
Y_L_test_no_noise=[];
Y_H_test=[];
Y_H_test_no_noise=[];
Y_H_test_R_predictDL=[];
Y_H_test_C_predictDL=[];
Y_H_test_predictDL=[];
Y_H_test_R=[];
Y_H_test_C=[];
Y_H_test_R_predict=[];
Y_H_test_C_predict=[];
Y_H_test_predict=[];
Y_L_test_R_act=[];
Y_L_test_C_act=[];
no_noise =0; % data with noise
norma_real=[]; 
norma_comp=[];
mean_Y_L_test_R=[]
mean_Y_L_test_C=[]

Y_L_test_R=[];
Y_L_test_norm=[];
Y_L_test_C=[];
Y_L_test_norm=[];
Y_H_test_R=[];
Y_H_test_norm=[];
Y_H_test_C=[];
Y_H_test_norm=[];
sigma_noise=1;
mean_central=1;
step = 5;
for j=1:trials
   first_angle=(angle_start+ceil((angle_end-angle_start)*rand(1)));%
   angle_new_loop1=first_angle:step:first_angle+step*(Target-1); 
   
    THETA_degree(:,j) =angle_new_loop1;
    THETA =(pi/180).*THETA_degree(:,j);  
    THETA_test{j}=THETA;
 
 real_tgt_ref_coe=ones(Target,1)';
  
 
    for i=1:L_Test %snapshots
        X =sqrt(real_tgt_ref_coe/2)'.*(randn(Target,P_L)+1i*randn(Target,P_L));
        [Y_L_test_temp(:,i)] = radar_mimo_data2awgn(P_L,M_L,N_L,THETA_test{j},X);
        [Y_H_test_temp(:,i)]= radar_mimo_data2awgn(P_H,M_H,N_H,THETA_test{j},X);
    end
    
    

    Y_L_test{j}=awgn(Y_L_test_temp,snr);
    Y_H_test{j}=awgn(Y_H_test_temp,snr);
    
 



end

Y_L_test_cell=Y_L_test; % actual low ant data
Y_H_test_cell=Y_H_test; % actual high ant data


% cascade data to get matrix input to DNN
Y_L_test_R=[];
Y_L_test_C=[];
Y_L_test_ori_R=[];
Y_L_test_ori_C=[];


for j=1:trials
  Y_L_test_ori_R=[Y_L_test_ori_R  real(Y_L_test_cell{j})];
 Y_L_test_ori_C=[Y_L_test_ori_C  imag(Y_L_test_cell{j})];

end


 filename=['D:\\Data\\Testing\\Testingnew_Cascade_SNR_' num2str(snr) num2str(Target) num2str(N) num2str(M)  '_' num2str(angle_start) '_' num2str(angle_end) '.mat'];

  save (filename,'Y_L_test_ori_R','Y_L_test_ori_C','Y_H_test_cell','Y_L_test_cell', 'Target','THETA_test','-v7.3');
end
end
end


