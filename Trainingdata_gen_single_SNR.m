% This file generates the training data for the training SNR range -16 to 10 SNRs. This file generates a single
% dateset file for each training SNR for each angle range. Here, we consider  five angle ranges (0-25), (10-35),(20-45),(30-55) and (40-65).
% Training data generated from this code is used to traing the DNN.
clear;clc;
rng('default')
tic

% Parameters Setting

 
 Total_data_tr_val     =   60000; % 45,000 training and 15,000 validation
 testing_data_size     =    Total_data_tr_val*0.2; %validation data size
 no_noise=0;
 N=10;
 M=16;
angle_start_loop=0:10:40;
angle_end_loop=25:10:65;
ant_loop=1;
snr_loop=-16:2:10;



for angle_loop=1:length(angle_start_loop)
    
for snr_index=1:length(snr_loop)
Y_L_train=[];
Y_H_train=[];
Y_L_test=[];
Y_H_test=[];
snr=snr_loop(snr_index);    
M_H=M(ant_loop); %Tx antennas
N_H=M(ant_loop); %RX antennas
M_L=N(ant_loop); %Tx antennas
N_L=N(ant_loop); %RX antennas

L_L=Total_data_tr_val; % number of data points
Target=4; % number of targets
angle_start=angle_start_loop(angle_loop);
angle_end=angle_end_loop(angle_loop);

real_tgt_ref_coe=ones(1,Target);   
% angle_start=0;
% angle_end=90;
THETA =  (pi/180).*( angle_start+ceil((angle_end-angle_start).*rand(L_L,Target)));%angles of each target
% THETA = randi([angle_start angle_end],[L_L,Target]);
P_H=1; %Pulses
M_H=M(ant_loop); %Tx antennas
N_H=M(ant_loop); %RX antennas
L_H=L_L; % number of data points
Y_H_temp=zeros(M_H*N_H,L_H);
Y_H_temp_no_noise=zeros(M_H*N_H,L_H);
phi_k=2*pi*rand(Target,P_H); %target phase (normalized to 1)


P_L=1; %Pulses
M_L=N(ant_loop); %Tx antennas
N_L=N(ant_loop); %RX antennas
Y_L_temp=zeros(M_L*N_L,L_L);
Y_L_temp_no_noise=zeros(M_L*N_L,L_L);



     
for i=1:L_H
  
    X =sqrt(real_tgt_ref_coe/2)'.*(randn(Target,P_H)+1i*randn(Target,P_H));
   [Y_H_temp(:,i)]=radar_mimo_data2awgn(P_H,M_H,N_H,THETA(i,:),X);
   [Y_L_temp(:,i)] = radar_mimo_data2awgn(P_L,M_L,N_L,THETA(i,:),X);
end

Y_H_temp=awgn(Y_H_temp,snr);
Y_L_temp=awgn(Y_L_temp,snr); 

if no_noise==1
    Y_H_temp=Y_H_temp_no_noise;
    Y_L_temp=Y_L_temp_no_noise;
end

 % split test and validation data
 split_point = round(L_L-testing_data_size);
 seq = randperm(L_L);

Y_L_train= Y_L_temp(:,seq(1:split_point));
Y_H_train=Y_H_temp(:,seq(1:split_point));

THETA_TR=THETA(seq(1:split_point),:);
 %  validation data
Y_L_test=Y_L_temp(:,seq(split_point+1:end));% lower antenna configuation
Y_H_test=Y_H_temp(:,seq(split_point+1:end));% higher antenna configuation


THETA_T = THETA(seq(split_point+1:end),:);  %angles of each target

%Y_L_train_R=real(Y_L_train);
%Y_L_train_C=imag(Y_L_train);
Y_L_train_R_ori=real(Y_L_train);
Y_L_train_C_ori=imag(Y_L_train);
Y_H_train_R=real(Y_H_train);
Y_H_train_C=imag(Y_H_train);


Y_L_val_R_ori=real(Y_L_test);
Y_L_val_C_ori=imag(Y_L_test);
Y_H_val_R=real(Y_H_test);
Y_H_val_C=imag(Y_H_test);







filename=['D:\\Data\\Trainingcascade_SNRalone' '_' num2str(snr) '_' num2str(Total_data_tr_val) '_' num2str(Target) num2str(M_L) num2str(M_H) '_' num2str(angle_start) '_' num2str(angle_end) '.mat'];
save(filename, 'Y_L_train_R_ori', 'Y_L_train_C_ori', 'Y_L_val_R_ori', 'Y_L_val_C_ori','Y_H_train_R','Y_H_train_C','Y_H_val_C','Y_H_val_R','-v7.3');

end


end






