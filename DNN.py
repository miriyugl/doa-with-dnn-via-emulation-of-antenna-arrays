#In this file DNN is trained to learn the mapping between received signals of high and low antenna setups.
#Input to the DNN is the received signals of low and high antenna setups([train_low_R train_low_C] and [train_high_R train_high_C])
#Once DNN is trainded, in the testing phase received signal of the low antenna setup ([test_low_R  test_low_C]) is used to predict the received signal of high antenna setup.

from keras.models import Model
from keras.layers import (Input, Dense)
import numpy as np
import hdf5storage
import scipy.io as sio
from sklearn import preprocessing
from keras.models import Model, Sequential
from keras import backend as K
import pdb
#angle ranges
irange  = range(0, 50, 10)
# number of targets
target=[4]
#traning SNR range
train_snr_loop=range(-16, 12, 2)
for i in irange:
    for snr_train in train_snr_loop:
        #clear the model and data from one SNR to another
        if  snr_train!=-16:
             del model
             K.clear_session()
        # load training data
        v = hdf5storage.loadmat(
            'D:\\Data\\Training\\Trainingcascade_SNR_' + str(snr_train) + '_50000_41016_' + str(i) + '_' + str(i + 25) + '.mat')
        mm_scalerRT_INPUT_H = preprocessing.MinMaxScaler()
        mm_scalerRT_INPUT_L = preprocessing.MinMaxScaler()
        # Low antenna data
        train_low_R = v['Y_L_train_R_ori']
        train_low_C = v['Y_L_train_C_ori']
        # cascade real and imginary data
        train_low=np.concatenate((train_low_R, train_low_C ), axis=0)
        train_low = np.transpose(train_low)
        #Normalization
        train_low =mm_scalerRT_INPUT_L.fit_transform(train_low)
        # validation data
        val_low_R= v['Y_L_val_R_ori']
        val_low_C = v['Y_L_val_C_ori']
        val_low=np.concatenate((val_low_R, val_low_C ), axis=0)
        val_low=np.transpose(val_low)
        val_low =mm_scalerRT_INPUT_L.transform(val_low)


        # high antenna data

        train_high_R = v['Y_H_train_R']
        train_high_C = v['Y_H_train_C']
        train_high =np.concatenate((train_high_R, train_high_C ), axis=0)
        train_high = np.transpose(train_high)

        train_high =mm_scalerRT_INPUT_H.fit_transform(train_high)
        print(xtrain_high.shape[0])
        val_high_R = v['Y_H_val_R']
        val_high_C = v['Y_H_val_C']
        val_high=np.concatenate((val_high_R, val_high_C ), axis=0)
        val_high=np.transpose(val_high)
        val_high=mm_scalerRT_INPUT_H.transform(val_high)
        ## define the keras model


        model = Sequential()

        model.add(Dense(train_low.shape[1], input_dim=train_low.shape[1], activation='relu'))
        model.add(Dense(int(train_low.shape[1]), activation='relu'))
        model.add(Dense(int(train_high.shape[1]), activation='relu'))
        model.add(Dense(train_high.shape[1], activation='relu'))
        # compile the keras model
        model.summary()
        model.compile(optimizer='adam', loss='mean_squared_error')
        # fit the keras model on the dataset
        model.fit(train_low, train_high,
                        epochs=300,
                        batch_size=128,
                        shuffle=True,
                        validation_data=(val_low, val_high))
        # testing
        targetnumrange = range(4, 5, 1)
        for jjj in targetnumrange:
            targetnum=jjj
            # testing SNR range
            isnr = range(-16, 12, 2)
            for j in isnr:
                t = hdf5storage.loadmat("D:\\Data\\Testing\\Testing_Cascade_SNR_" + str(j)+ str(targetnum)+  "1016_" + str(i) +  "_"+ str(i+10) + ".mat")
                test_low_R = t['Y_L_test_ori_R']
                test_low_C= t['Y_L_test_ori_C']
                # Normalization
                mm_scalerRTEST_INPUT = preprocessing.MinMaxScaler()
                #cascade real and imginary data
                test_low_R = np.concatenate((test_low_R, y_testLC_UN), axis=0)
                test_low = test_low.transpose()
                test_low = mm_scalerRT_INPUT_L.transform(test_low)

                print(test_low.shape[1])
                print(test_low.shape[0])
                # prediction
                test_H_predict_nor = model.predict(test_low)
                # Denormalization
                predict_H_all = mm_scalerRT_INPUT_H.inverse_transform(test_H_predict_nor)
                predict_H_all = predict_H_all.transpose()
                [predict_H_R, predict_H_C] = np.vsplit(predict_H_all, 2)
                # save prdiction data (input to the MUSIC algorithm to estimate DOA)
                predict = {}
                predict['Y_H_test_predictC'] = predict_H_C
                predict['Y_H_test_predictR'] = predict_H_R
                savefilename=("D:\\Data\\prediction\\Predict_cascadeDNN_snr"+str(snr_train)+str(j)+ str(targetnum)+ str(i) + str(i+25) + "_1016.mat")
                sio.savemat(savefilename, predict)
                print('finish')
                print(j)
                print(savefilename)



