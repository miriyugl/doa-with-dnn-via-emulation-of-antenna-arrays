% Music algorithm for DoA estimation
function [pksm,locsm,theta_sample,music_spectrum] = radar_mimo_doa(Y,M,N,Ns, Targets)
%Y received signal
%M Transmit antenna
%N Receive antenna
%Ns number of samples
% Targets are number of sources
R=zeros(N*M,N*M);
% for i=1:Ns
   
R=R+(Y*Y');
% end
R=R/Ns;

%% calculate output beam pattern
theta_sample = [-90:0.5:90].*pi./180 ;
beam = zeros(1,length(theta_sample));
aT_Virtual=zeros(M*N,1);
aT_virtual_vec=zeros(1,M*N);
aT_sample = zeros(N,length(theta_sample)); 
for i=1:length(theta_sample)
   for j=1:N
      aT_sample(j,i) = exp(1i*pi*(j-1)*sin(theta_sample(i))).';
   end  
   aT_Virtual(:,i)=kron(aT_sample(:,i), aT_sample(:,i));
%    aT_virtual_vec(:,i)=aT_Virtual(:);
end  

K=Targets;
r=K;
% R=inv(R);
[Q ,D]=eig(R); %Compute eigendecomposition of covariance matrix
[D,I]=sort(diag(D),1,'descend'); %Find r largest eigenvalues
Q=Q (:,I); %Sort the eigenvectors to put signal eigenvectors first
Qs=Q (:,1:r); %Get the signal eigenvectors
Qn=Q(:,r+1:N*M); %Get the noise eigenvectors
% MUSIC algorithm

for l=1:length(theta_sample)
%Compute MUSIC spectrum
music_spectrum(l)=(aT_Virtual(:,l)'*aT_Virtual(:,l))/(aT_Virtual(:,l)'*Qn*Qn'*aT_Virtual(:,l));
end
[pksm,locsm]=findpeaks(abs(music_spectrum),theta_sample.*180./pi,'Threshold',.005);
