% this file is used to estimate the DoA using the MUSIC algorithm and calculate the DoA estimation error.
clear;clc;
% angle loop
angle_start_loop=0:10:40;
angle_end_loop=10:10:50;
normalisation = 0;
target_loop=[4]
snr_loop_train=-16:2:10;% trainng snr loop
 
for target_loop_index=1:length(target_loop)
for angle_loop=5:length(angle_start_loop) 
angle_start=angle_start_loop(angle_loop);
angle_end=angle_end_loop(angle_loop);
Target=target_loop(target_loop_index);
Total_data_tr_val=50000;
 N=10;
 M=16;
 P_L=1
 P_H=1
 snr_loop=-16:2:10 % testing snr 
 

for snr_loop_test=1:length(snr_loop)
snr=snr_loop(snr_loop_test) % TESTING SNR
filename=['D:\\Data\\Testing\\Testing\\Testingnew_Cascade_SNR_' num2str(snr) num2str(Target) num2str(N) num2str(M)  '_' num2str(angle_start) '_' num2str(angle_end) '.mat'];
load (filename)
% calculates angle for high and low

pksmL=[];
locsmL=[];
theta_sample_H=[];
music_spectrumL=[];
pksmH=[];
locsmH=[];
theta_sample_L=[];
music_spectrumH=[];
theta_all=[];
M_L=sqrt(size(Y_L_test_cell{1},1));
N_L=M_L;
M_H=sqrt(size(Y_H_test_cell{1},1));
N_H=M_H;
Ns=size(Y_L_test_ori_C,2)/ size(Y_H_test_cell,2);
snapshots=Ns;
% Doa estimation using MUSIC.
for j=1:size(THETA_test,2)

[pksmL{j},locsmL{j},theta_sample_L{j},music_spectrumL{j}] = radar_mimo_doa(Y_L_test_cell{j},M_L,N_L,Ns,Target);
[pksmH{j},locsmH{j},theta_sample_H{j},music_spectrumH{j}] = radar_mimo_doa(Y_H_test_cell{j},M_H,N_H,Ns,Target);
locsmL{j}=locsmL{j}.*pi./180;
locsmH{j}=locsmH{j}.*pi./180;
actualtheta=sort(THETA_test{j}.','descend');
theta_all=[theta_all actualtheta];
length_actual_theta=length(actualtheta);
% sort based on closest angle

[minValue,closestIndexL] = min(abs(locsmL{j}-actualtheta.'));

[minValue,closestIndexH] = min(abs(locsmH{j}-actualtheta.'));

locsmL_temp_min=[]
if length(closestIndexL) ~= length(unique(closestIndexL))
    closestIndexL=[];
    locsmL_temp=locsmL{j};
    for index_temp=1:length(locsmL_temp)
     locsmL_temp_min(index_temp)=min(abs(locsmL_temp(index_temp)-actualtheta.'))
    end
    
    [~, II]=sort(locsmL_temp_min,'ascend')
    actualtheta_temp=actualtheta;
    for index_temp=1:length(locsmL_temp)
        [minValue,closestIndexL(index_temp)] = min(abs(locsmL_temp(II(index_temp))-actualtheta_temp.'));
        actualtheta_temp(actualtheta_temp==actualtheta_temp(closestIndexL(index_temp))) = 1000;
    end
    
 clear II   
end

newsize=length(actualtheta);%max([L,H,HPD,HP,length(actualtheta)]);
L_vec=zeros(1,newsize);
L_vec(closestIndexL)=locsmL{j};
L_vec=sort(L_vec,'descend');

locsmH_temp_min=[];
if length(closestIndexH) ~= length(unique(closestIndexH))
    closestIndexH=[];
    locsmH_temp=locsmH{j};
    for index_temp=1:length(locsmH_temp)
     locsmH_temp_min(index_temp)=min(abs(locsmH_temp(index_temp)-actualtheta.'))
    end
    
    [~, II]=sort(locsmH_temp_min,'ascend')
    actualtheta_temp=actualtheta;
    for index_temp=1:length(locsmH_temp)
        [minValue,closestIndexH(index_temp)] = min(abs(locsmH_temp(II(index_temp))-actualtheta_temp.'));
        actualtheta_temp(actualtheta_temp==actualtheta_temp(closestIndexH(index_temp))) = 1000;
    end
    
   clear II   
end



H_vec=zeros(1,newsize);
H_vec(closestIndexH)=locsmH{j};
H_vec=sort(H_vec,'descend');
% calculate RMSE for actual High and Low antenna setups
L_rmse(j)=(sum((actualtheta-L_vec).^2));
H_rmse(j)=(sum((actualtheta-H_vec).^2));
L_rmse_norm(j)=norm((actualtheta-L_vec ), 'fro') / norm(actualtheta, 'fro');
H_rmse_norm(j)=norm((actualtheta-H_vec ), 'fro') / norm(actualtheta, 'fro');

end
% calculate average RMSE for actual High and Low antenna setups

Error_L_act_norm(angle_loop,snr_loop_test)=mean(L_rmse_norm);
Error_H_act_norm(angle_loop,snr_loop_test)=mean(H_rmse_norm);
Error_L(angle_loop,snr_loop_test)=(mean(L_rmse))/newsize;
Error_H(angle_loop,snr_loop_test)=(mean(H_rmse))/newsize;

for snr_index=1:length(snr_loop_train)
snr_train=snr_loop_train(snr_index)

% LOAD PREDICTION FILE Based on DNN 
predictfilename=['D:\\Data\\prediction\\Predict_cascadeDNN_snr' num2str(snr_train) num2str(snr) num2str(Target)  num2str(angle_start)  num2str(angle_start+25) '_1016.mat'];
load (predictfilename)
Y_H_test_predict_only=Y_H_test_predictR+1i*Y_H_test_predictC;

pksmHP_only=[];
locsmHP_only=[];
music_spectrumHP_only=[];
theta_all=[];
%DOA estimation for predicted data
for j=1:size(THETA_test,2)

[pksmHP_only{j},locsmHP_only{j},theta_sample_only{j},music_spectrumHP_only{j}] = radar_mimo_doa(Y_H_test_predict_only(:,1+(j-1)*snapshots:j*snapshots),M_H,N_H,Ns,Target);
actualtheta=sort(THETA_test{j}.','descend');
theta_all=[theta_all actualtheta];
length_actual_theta=length(actualtheta);
locsmHP_only{j}=locsmHP_only{j}.*pi./180;

[minValue,closestIndexHP] = min(abs(locsmHP_only{j}-actualtheta.'));

locsmHP_temp_min=[]
 if length(closestIndexHP) ~= length(unique(closestIndexHP))
    locsmHP_temp=locsmHP_only{j};
    closestIndexHP=[];
    for index_temp=1:length(locsmHP_temp)
     locsmHP_temp_min(index_temp)=min(abs(locsmHP_temp(index_temp)-actualtheta.'))
    end
    
    [~, II]=sort(locsmHP_temp_min,'ascend')
    actualtheta_temp=actualtheta;
    for index_temp=1:length(locsmHP_temp)
        [minValue,closestIndexHP(index_temp)] = min(abs(locsmHP_temp(II(index_temp))-actualtheta_temp.'));
        actualtheta_temp(actualtheta_temp==actualtheta_temp(closestIndexHP(index_temp))) = 1000;
    end
    
  clear II    
 end
newsize=length(actualtheta);%max([L,H,HPD,HP,length(actualtheta)]);
HP_only_vec(closestIndexHP)=locsmHP_only{j};
HP_only_vec=sort(HP_only_vec,'descend');
 

HP_only_rmse(j)=(sum((actualtheta-HP_only_vec).^2));
HP_only_rmse_norm(j)=norm((actualtheta-HP_only_vec), 'fro') / norm(actualtheta, 'fro');


end
% calculate average MSE for predicted data
Error_HPD_only_act_norm(snr_loop_test,snr_index)=mean(HP_only_rmse_norm);
Error_HPD_only_act(snr_loop_test,snr_index)=mean(HP_only_rmse)/newsize;
end
% finding rhe lowest error of the predicted data
min_HPD=min(Error_HPD_only_act_norm(snr_loop_test,:));
Error_HPD_only_rel(snr_loop_test,:)=Error_HPD_only_act_norm(snr_loop_test,:)-min_HPD;
Error_HPD_only_rel2(snr_loop_test,:)=(Error_HPD_only_act_norm(snr_loop_test,:)-min_HPD)./min_HPD;

end

Error_HPD_only_act_norm_all(angle_loop,:,:)=Error_HPD_only_act_norm;
Error_HPD_only_act_all(angle_loop,:,:)=Error_HPD_only_act;

end

end






