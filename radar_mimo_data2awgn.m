
% data generation based on MIMO radar model
function [Y] = radar_mimo_data2awgn(P,M,N,THETA,X)



K=length(THETA); %Target
for c=1:K
Steering_vec_tx(:,c)=exp(1i*pi.*(0:M-1)*sin(THETA(c)));
Steering_vec_rx(:,c)=exp(1i*pi.*(0:N-1)*sin(THETA(c)));
A_theta(:,c)=kron(Steering_vec_rx(:,c),Steering_vec_tx(:,c));
end



Y=A_theta*X;



end

